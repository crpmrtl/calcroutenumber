﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Excel = Microsoft.Office.Interop.Excel;

namespace CalcRouteNumber {
    public partial class frmMain : Form {
        private const int COL_TRAN_COUNT = 0;
        private const int COL_AVG_RESPONSE = 1;
        private const int COL_ROUTE_NUM = 2;

        private DataGridViewCellStyle cellStyle;
        private DataGridViewCellStyle defaultCellStyle;

        public frmMain() {
            InitializeComponent();
        }

        private void frmMain_Load(object sender, EventArgs e) {
            cellStyle = new DataGridViewCellStyle();
            cellStyle.Font = new Font(tblMain.Font, tblMain.Font.Style | FontStyle.Bold);
            cellStyle.ForeColor = Color.Red;

            defaultCellStyle = new DataGridViewCellStyle();
            defaultCellStyle.Font = new Font(tblMain.Font, tblMain.Font.Style);
        }

        private void onClickBtnCalc(object sender, EventArgs e) {
            for (int row = 0; row < tblMain.RowCount; ++row) {
                if (tblMain[COL_TRAN_COUNT, row].Value != null
                    && tblMain[COL_AVG_RESPONSE, row].Value != null
                    && (tblMain[COL_ROUTE_NUM, row].Value == null
                    || tblMain[COL_ROUTE_NUM, row].Style == cellStyle)) {
                    try {
                        tblMain[COL_TRAN_COUNT, row].Style = defaultCellStyle;
                        tblMain[COL_AVG_RESPONSE, row].Style = defaultCellStyle;
                        tblMain[COL_ROUTE_NUM, row].Style = cellStyle;
                        tblMain[COL_ROUTE_NUM, row].Value = "計算中";
                        tblMain.Update();

                        int tranCount = int.Parse(tblMain[COL_TRAN_COUNT, row].Value.ToString());
                        int avgResponse = int.Parse(tblMain[COL_AVG_RESPONSE, row].Value.ToString());

                        int result = calcRouteNumber(tranCount, avgResponse);

                        tblMain[COL_ROUTE_NUM, row].Value = result > 0 ? "" + result : "計算不可";

                    } catch (FormatException ex) {
                        tblMain[COL_ROUTE_NUM, row].Value = ex.Message;
                    }
                } else if ((tblMain[COL_TRAN_COUNT, row].Value == null
                    || tblMain[COL_TRAN_COUNT, row].Style == cellStyle)
                    && tblMain[COL_AVG_RESPONSE, row].Value != null
                    && tblMain[COL_ROUTE_NUM, row].Value != null) {
                    try {
                        tblMain[COL_TRAN_COUNT, row].Style = cellStyle;
                        tblMain[COL_AVG_RESPONSE, row].Style = defaultCellStyle;
                        tblMain[COL_ROUTE_NUM, row].Style = defaultCellStyle;
                        tblMain[COL_TRAN_COUNT, row].Value = "計算中";
                        tblMain.Update();

                        int avgResponse = int.Parse(tblMain[COL_AVG_RESPONSE, row].Value.ToString());
                        int routeNum = int.Parse(tblMain[COL_ROUTE_NUM, row].Value.ToString());

                        int result = searchTranCount(avgResponse, routeNum);

                        tblMain[COL_TRAN_COUNT, row].Value = result > 0 ? "" + result : "計算不可";
                    } catch (FormatException ex) {
                        tblMain[COL_TRAN_COUNT, row].Value = ex.Message;
                    }
                }
            }
        }

        private int searchTranCount(int avgResponse, int routeNum) {
            int tranCount;

            int tranMin = 1, tranMax = -1;

            tranCount = 10000;
            while (true) {
                int route = calcRouteNumber(tranCount, avgResponse);
                if (route < 0) {
                    tranMax = tranCount;
                    tranCount = (tranMax + tranMin) / 2;
                    continue;
                }
                if (route <= routeNum) {
                    tranMin = tranCount;
                    if (tranMax < 0) {
                        tranCount *= 2;
                    } else {
                        tranCount = (tranMax + tranMin) / 2;
                    }
                } else {
                    tranMax = tranCount;
                    if (tranMin < 0) {
                        tranCount /= 2;
                    } else {
                        tranCount = (tranMax + tranMin) / 2;
                    }
                }
                if (tranMax - tranMin == 1) {
                    tranCount = tranMin;
                    break;
                }
            }

            return tranCount;
        }

        private int calcRouteNumber(int tranCount, int avgResponse) {
            double expr1 = 1.0;
            double callNumber = (double)avgResponse * (double)tranCount / (60 * 60 * 1000);

            int routeNumber;

            bool isBreak = false;

            for (routeNumber = 1; routeNumber < 1000; ++routeNumber) {
                double expr = expression1(callNumber, routeNumber);
                expr1 += expr;

                if (1.0 > tranCount * expr / expr1) {
                    isBreak = true;
                    break;
                }
            }

            return isBreak ? routeNumber : -1;
        }

        private double expression1(double callNumber, int routeNumber) {
            return Math.Pow(callNumber, routeNumber) / fact(routeNumber);
        }

        private double fact(int n) {
            double fact = 1.0;

            for (int i = n; i > 1; --i) {
                fact *= i;
            }

            return fact;
        }

        private void onClickBtnLoad(object sender, EventArgs e) {
            string appPath = Application.StartupPath;
            string confFilename = appPath + @"\config.xml";

            Settings settings = null;
            try {
                System.Xml.Serialization.XmlSerializer serializer = new System.Xml.Serialization.XmlSerializer(typeof(Settings));
                System.IO.StreamReader sr = new System.IO.StreamReader(confFilename, new System.Text.UTF8Encoding(false));
                settings = (Settings)serializer.Deserialize(sr);
                sr.Close();
            } catch (Exception ex) {
                if (ex is System.IO.FileNotFoundException ||
                    ex is InvalidOperationException) {
                    settings = new Settings();
                    settings.LastPath = appPath;
                } else {
                    throw ex;
                }
            }

            OpenFileDialog dialog = new OpenFileDialog();
            dialog.InitialDirectory = settings.LastPath;
            dialog.Multiselect = false;
            dialog.Filter = "Excel Files(*.xls;*.xlsx;*.xlsm;*.xlsb)|*.xls;*.xlsx;*.xlsm;*.xlsb";

            if (dialog.ShowDialog().Equals(DialogResult.OK)) {
                String loadFilename = dialog.FileName;

                string loadFileDir = System.IO.Path.GetDirectoryName(loadFilename);

                settings.LastPath = loadFileDir;

                System.Xml.Serialization.XmlSerializer serializer = new System.Xml.Serialization.XmlSerializer(typeof(Settings));
                System.IO.StreamWriter sw = new System.IO.StreamWriter(confFilename, false, new System.Text.UTF8Encoding(false));
                serializer.Serialize(sw, settings);
                sw.Close();

                Excel.Application oXls = new Excel.Application();
                Excel.Workbook oWorkbook = (Excel.Workbook)(oXls.Workbooks.Open(
                    Filename: loadFilename,
                    ReadOnly: true
                    ));
                Excel.Worksheet oSheet = oWorkbook.Sheets[1];

                tblMain.Rows.Clear();

                int row = 1;
                String tranCount, avgResponse;
                while(true) {
                    tranCount = oSheet.Cells[row, 1].Text.ToString();
                    avgResponse = oSheet.Cells[row, 2].Text.ToString();

                    if (tranCount.Equals("") || avgResponse.Equals("")) {
                        break;
                    }

                    tblMain.Rows.Add();
                    tblMain[COL_TRAN_COUNT, row - 1].Value = tranCount;
                    tblMain[COL_AVG_RESPONSE, row - 1].Value = avgResponse;

                    ++row;
                }

                oWorkbook.Close();
                oXls.Quit();
            }
        }
    }
}
