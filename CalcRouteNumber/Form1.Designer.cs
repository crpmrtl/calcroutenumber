﻿namespace CalcRouteNumber
{
    partial class frmMain
    {
        /// <summary>
        /// 必要なデザイナー変数です。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 使用中のリソースをすべてクリーンアップします。
        /// </summary>
        /// <param name="disposing">マネージ リソースを破棄する場合は true を指定し、その他の場合は false を指定します。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows フォーム デザイナーで生成されたコード

        /// <summary>
        /// デザイナー サポートに必要なメソッドです。このメソッドの内容を
        /// コード エディターで変更しないでください。
        /// </summary>
        private void InitializeComponent()
        {
            this.btnCalc = new System.Windows.Forms.Button();
            this.tblMain = new System.Windows.Forms.DataGridView();
            this.btnLoadFile = new System.Windows.Forms.Button();
            this.colTranCount = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colAvgResponse = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colResult = new System.Windows.Forms.DataGridViewTextBoxColumn();
            ((System.ComponentModel.ISupportInitialize)(this.tblMain)).BeginInit();
            this.SuspendLayout();
            // 
            // btnCalc
            // 
            this.btnCalc.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCalc.Location = new System.Drawing.Point(405, 86);
            this.btnCalc.Name = "btnCalc";
            this.btnCalc.Size = new System.Drawing.Size(76, 23);
            this.btnCalc.TabIndex = 5;
            this.btnCalc.Text = "計算する！";
            this.btnCalc.UseVisualStyleBackColor = true;
            this.btnCalc.Click += new System.EventHandler(this.onClickBtnCalc);
            // 
            // tblMain
            // 
            this.tblMain.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tblMain.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.tblMain.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.colTranCount,
            this.colAvgResponse,
            this.colResult});
            this.tblMain.Location = new System.Drawing.Point(12, 12);
            this.tblMain.Name = "tblMain";
            this.tblMain.RowTemplate.Height = 21;
            this.tblMain.Size = new System.Drawing.Size(387, 257);
            this.tblMain.TabIndex = 8;
            // 
            // btnLoadFile
            // 
            this.btnLoadFile.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnLoadFile.Location = new System.Drawing.Point(405, 12);
            this.btnLoadFile.Name = "btnLoadFile";
            this.btnLoadFile.Size = new System.Drawing.Size(76, 23);
            this.btnLoadFile.TabIndex = 5;
            this.btnLoadFile.Text = "読込";
            this.btnLoadFile.UseVisualStyleBackColor = true;
            this.btnLoadFile.Click += new System.EventHandler(this.onClickBtnLoad);
            // 
            // colTranCount
            // 
            this.colTranCount.HeaderText = "1時間取引件数";
            this.colTranCount.MaxInputLength = 10;
            this.colTranCount.Name = "colTranCount";
            this.colTranCount.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.colTranCount.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.colTranCount.Width = 110;
            // 
            // colAvgResponse
            // 
            this.colAvgResponse.HeaderText = "平均レスポンス(ms)";
            this.colAvgResponse.MaxInputLength = 10;
            this.colAvgResponse.Name = "colAvgResponse";
            this.colAvgResponse.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.colAvgResponse.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.colAvgResponse.Width = 110;
            // 
            // colResult
            // 
            this.colResult.DividerWidth = 2;
            this.colResult.HeaderText = "経路数";
            this.colResult.Name = "colResult";
            this.colResult.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.colResult.Width = 110;
            // 
            // frmMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(493, 281);
            this.Controls.Add(this.tblMain);
            this.Controls.Add(this.btnLoadFile);
            this.Controls.Add(this.btnCalc);
            this.MaximizeBox = false;
            this.Name = "frmMain";
            this.Text = "必要経路数計算機";
            this.Load += new System.EventHandler(this.frmMain_Load);
            ((System.ComponentModel.ISupportInitialize)(this.tblMain)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Button btnCalc;
        private System.Windows.Forms.DataGridView tblMain;
        private System.Windows.Forms.Button btnLoadFile;
        private System.Windows.Forms.DataGridViewTextBoxColumn colTranCount;
        private System.Windows.Forms.DataGridViewTextBoxColumn colAvgResponse;
        private System.Windows.Forms.DataGridViewTextBoxColumn colResult;
    }
}

