﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CalcRouteNumber {
    public class Settings {
        private String lastPath;

        public string LastPath {
            get {
                return lastPath;
            }

            set {
                lastPath = value;
            }
        }
    }
}
